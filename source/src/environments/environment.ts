export const environment = {
    apiBaseUrl: 'http://t-challenge-service-dev.openshift.devops.t-systems.ru',
    clientBaseUrl: 'http://t-challenge-participant-dev.openshift.devops.t-systems.ru',
    production: false
};
