export const environment = {
    apiBaseUrl: 'http://t-challenge-service-prod.openshift.devops.t-systems.ru',
    clientBaseUrl: 'http://t-challenge-participant-prod.openshift.devops.t-systems.ru',
    production: true
};
