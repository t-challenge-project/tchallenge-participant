FROM nginx:latest

COPY source/dist/. /usr/share/nginx/html/

EXPOSE 4200
